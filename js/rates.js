Tomizone.RatesView = Ember.View.extend({
    templateName: "rates",
    classNames: ['form-group col-xs-12']
});

Tomizone.RateView = Ember.View.extend({
    tagName: 'td',
    templateName: "rate",

    rate: null,
    selectedRateId: null,
    popularRateId: null,

    selected: function() {
        return this.get("rate.id") == this.get("selectedRateId");
    }.property("rate", "selectedRateId"),

    popular: function() {
        return this.get("rate.id") == this.get("popularRateId");
    }.property("rate", "popularRateId")
});
