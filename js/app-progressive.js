Tomizone.setupAttrInputs = function(attributes) {
  var attrs = {};
  $.each(attributes, function(index, attr) {
    attrs[attr.name] = attr.data;
  });

  var getOnlineForm = $("form#getonline");
  if (getOnlineForm && getOnlineForm.length) {
    // Build a map of attributes for easy index
    var totalCount = 0;
    var showCount = 0;
    var hideCount = 0;
    var maxShowCount = 2;
    $(".attr-input", getOnlineForm).each(function(index) {
      var $el = $(this);
      // have we reached our max count or is the value populated hide
      if (showCount == maxShowCount || $el.data("attribute") && attrs[$el.data("attribute")]) {
        // have to force change by setting val which forces validation
        hideCount++;
        $el.hide().trigger("hide");
      } else {
        showCount++;
        $el.show();
      }
      totalCount++;
    });
    if (totalCount == hideCount)
      setTimeout(function() {
        getOnlineForm.submit();
      }, 5000);

    else
      $(".display-profile", getOnlineForm).show().removeClass("hidden");
  }

  return attrs;
};

Tomizone.onAttributesLoaded = function(attributes) {

  var attrs = Tomizone.setupAttrInputs(attributes);

};
