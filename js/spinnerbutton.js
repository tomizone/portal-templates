
Tomizone.SpinSpinnerComponent = Ember.Component.extend({
    classNames: ['spin-spinner'],

    attributeBindings: ['style'],

    lines: 13,

    length: 20,

    width: 10,

    radius: 30,

    corners: 1,

    rotate: 0,

    direction: 1,

    speed: 1,

    trail: 60,

    shadow: false,

    hwaccel: false,

    color: null,

    left: '50%',

    top: '50%',

    style: function() {
        return 'position: absolute; width: 0; height: 0; left: ' + this.get('left') + '; top: ' + this.get('top') + ';';
    }.property('top', 'left'),

    startSpinner: function() {
        this.$().spin({
            lines: this.get('lines'),
            length: this.get('length'),
            width: this.get('width'),
            radius: this.get('radius'),
            corners: this.get('corners'),
            rotate: this.get('rotate'),
            direction: this.get('direction'),
            speed: this.get('speed'),
            trail: this.get('trail'),
            shadow: this.get('shadow'),
            hwaccel: this.get('hwaccel'),
            left: 'auto',
            top: 'auto'
        }, this.get('color'));
    }.on('didInsertElement'),

    stopSpinner: function() {
        this.$().data().spinner.stop();
    }.on('willDestroyElement')
});

Tomizone.SpinnerButtonComponent = Ember.Component.extend({
    attributeBindings: ['style', 'type', 'tabindex', 'disabled', 'data-toggle', 'data-placement'],

    tagName: 'button',

    classNames: ['spinner-button'],

    classNameBindings: ['spinning'],

    isSpinning: false,

    action: null,

    type: "submit",

    disabled: false,

    tabindex: 1,

    lines: 8,

    radius: 5,

    length: 4,

    width: 3,

    corners: 1,

    rotate: 0,

    direction: 1,

    speed: 1,

    trail: 60,

    shadow: false,

    hwaccel: false,

    color: null,

    left: '50%',

    top: '50%',

    style: 'position: relative;',

    'data-toggle': "popover",

    'data-placement': "auto top",

    click: function() {
        if (this.get('isSpinning')) {
            //this.sendAction();
        }
    },

    maintainButtonDimensions: function() {
        if (this.get('isSpinning')) {
            this.set('style', 'position: relative; width: ' + this.$().outerWidth() + 'px; height: ' + this.$().outerHeight() +
                'px;');
            this.set("disabled", true);
        } else {
            this.set('style', null);
            this.set("disabled", false);
        }
    }.observes('isSpinning').on('didInsertElement')
});
