var Tomizone = Tomizone || {};

Tomizone.onApplicationComplete = function() {

  App.ApplicationRoute.reopen({

    actions: {
      showMenu: function() {
        var $body = $(document.body);
        if ($body.hasClass('menu-active'))
          $body.removeClass('menu-active');
        else
          $body.addClass('menu-active');
      },
      hideMenu: function() {
        var $body = $(document.body);
        if ($body.hasClass('menu-active'))
          $body.removeClass('menu-active');
      }
    }

  });

  App.ApplicationController.reopen({

    // Hide the menu as we navigate
    currentPathDidChange: function() {
      this.hideMenu();
      // Check if this is index path and run attributes
      if (this.get("currentPath") == "index") {
        var attributes = this.get('attributes.content');
        Ember.run.scheduleOnce('afterRender', this, function() {
          Tomizone.setupAttrInputs(attributes);
        });
      }
    }.observes('currentPath', 'url'),

    isSuccessPath: function() {
      return this.get('currentPath') == 'success';
    }.property('currentPath', 'url'),

    hideMenu: function() {
      var $body = $(document.body);
      if ($body.hasClass('menu-active'))
        $body.removeClass('menu-active');
    }

  });

  // Remove menu on menu item click
  $("body").on('click', "a.menu-item.active", function(e) {
    var $body = $(document.body);
    if ($body.hasClass('menu-active'))
      $body.removeClass('menu-active');
  });

};


//------------------------------------------------------
// Extensions
//------------------------------------------------------


