var gulp =              require('gulp');
var gutil =             require('gulp-util');
var rename =            require("gulp-rename");
var del =               require('del');
var zip =               require('gulp-zip');
var runSequence =       require('run-sequence');

var themes = [
    {
        folder:"110_Branded_OneClickOnline",
        config:"config-110.js.template",
        files:[
            // Standard
            '*css/normalise.css',
            '*css/style.css',
            '*img/background-lg.jpg',
            '*img/background-sm.jpg',
            '*img/lgo-tomizone.svg',
            '*partials/_header.hbs',
            '*partials/_footer.hbs',
            '*application.hbs',
            '*templates/error.hbs',
            '*templates/terms.hbs',
            '*templates/index.hbs',

            // exclude
            '!**/gulpfile.js',
            '!**/package.json',
            '!**/node_modules/*'
        ],
        renameFiles:[
            // Success template
            {
                from:'*templates/success-110.hbs',
                to:'/templates',
                name:'success.hbs'
            }
        ]
    },
    {
        folder:"111_Branded_OneClickOnline_Redirect",
        config:"config-111.js.template",
        files:[
            // Standard
            '*css/normalise.css',
            '*css/style.css',
            '*img/background-lg.jpg',
            '*img/background-sm.jpg',
            '*img/lgo-tomizone.svg',
            '*partials/_header.hbs',
            '*partials/_footer.hbs',
            '*application.hbs',
            '*templates/error.hbs',
            '*templates/terms.hbs',
            '*templates/index.hbs',

            // exclude
            '!**/gulpfile.js',
            '!**/package.json',
            '!**/node_modules/*'
        ],
        renameFiles:[
            // Success template
            {
                from:'*templates/success-111.hbs',
                to:'/templates',
                name:'success.hbs'
            }
        ]
    },
    {
        folder:"112_Branded_OneClickOnline_Advertising_Redirect",
        config:"config-112.js.template",
        files:[
            // Standard
            '*css/normalise.css',
            '*css/style.css',
            '*img/background-lg.jpg',
            '*img/background-sm.jpg',
            '*img/lgo-tomizone.svg',
            '*partials/_header.hbs',
            '*partials/_footer.hbs',
            '*application.hbs',
            '*templates/error.hbs',
            '*templates/terms.hbs',
            '*templates/index.hbs',

            // Advertising css
            '*css/adds.css',

            // Advertising images
            '*img/300-ad.png',
            '*img/960-ad.png',

            // exclude
            '!**/gulpfile.js',
            '!**/package.json',
            '!**/node_modules/*'
        ],
        renameFiles:[
            // Success template
            {
                from:'*templates/success-112.hbs',
                to:'/templates',
                name:'success.hbs'
            }
        ]
    },
    {
        folder:"210_Branded_App_Download",
        config:"config-210.js.template",
        files:[
            // Standard
            '*css/normalise.css',
            '*css/style.css',
            '*img/background-lg.jpg',
            '*img/background-sm.jpg',
            '*img/lgo-tomizone.svg',
            '*partials/_header.hbs',
            '*partials/_footer.hbs',
            '*application.hbs',
            '*templates/error.hbs',
            '*templates/terms.hbs',
            '*templates/index.hbs',

            // Download button css
            '*css/appdownload.css',

            // OS dependant partials
            '*partials/_appDownloadButton.hbs',
            '*partials/_appDownloadButtonANDROID.hbs',
            '*partials/_appDownloadButtonIOS.hbs',
            '*partials/_appDownloadButtonLINUX.hbs',
            '*partials/_appDownloadButtonMAC.hbs',
            '*partials/_appDownloadButtonWIN.hbs',
            '*partials/_appDownloadButtonWINMOBILE.hbs',


            // Application images
            '*img/300-app.png',
            '*img/960-app.png',

            // exclude
            '!**/gulpfile.js',
            '!**/package.json',
            '!**/node_modules/*'
        ],
        renameFiles:[
            // Success template
            {
                from:'*templates/success-210.hbs',
                to:'/templates',
                name:'success.hbs'
            }
        ]
    },
    {
        folder:"220_Branded_Suggest_Content",
        config:"config-220.js.template",
        files:[
            // Standard
            '*css/normalise.css',
            '*css/style.css',
            '*img/background-lg.jpg',
            '*img/background-sm.jpg',
            '*img/lgo-tomizone.svg',
            '*partials/_header.hbs',
            '*partials/_footer.hbs',
            '*application.hbs',
            '*templates/error.hbs',
            '*templates/terms.hbs',
            '*templates/index.hbs',

            // exclude
            '!**/gulpfile.js',
            '!**/package.json',
            '!**/node_modules/*'
        ],
        renameFiles:[
            // Success template
            {
                from:'*templates/success-220.hbs',
                to:'/templates',
                name:'success.hbs'
            }
        ]
    },
    {
        folder:"221_Branded_Video_Play",
        config:"config-221.js.template",
        files:[
            // Standard
            '*css/normalise.css',
            '*css/style.css',
            '*img/background-lg.jpg',
            '*img/background-sm.jpg',
            '*img/lgo-tomizone.svg',
            '*partials/_header.hbs',
            '*partials/_footer.hbs',
            '*application.hbs',
            '*templates/error.hbs',
            '*templates/terms.hbs',
            '*templates/index.hbs',

            // Video-js
            '*css/video-js.css',
            '*css/video.css',
            '*js/video.js',

            // Videos
            '*video/big_buck_bunny.mp4',
            '*video/big_buck_bunny.ogv',
            '*video/big_buck_bunny.webm',

            // Video poster image
            '*img/big_buck_bunny-poster.jpg',

            // exclude
            '!**/gulpfile.js',
            '!**/package.json',
            '!**/node_modules/*'
        ],
        renameFiles:[
            // Success template
            {
                from:'*templates/success-221.hbs',
                to:'/templates',
                name:'success.hbs'
            }
        ]
    },
    {
        folder:"310_Branded_Require_App_Download",
        config:"config-310.js.template",
        files:[
            // Standard
            '*css/normalise.css',
            '*css/style.css',
            '*img/background-lg.jpg',
            '*img/background-sm.jpg',
            '*img/lgo-tomizone.svg',
            '*partials/_header.hbs',
            '*partials/_footer.hbs',
            '*application.hbs',
            '*templates/error.hbs',
            '*templates/terms.hbs',


            // exclude
            '!**/gulpfile.js',
            '!**/package.json',
            '!**/node_modules/*'
        ],
        renameFiles:[
            // Index template
            {
                from:'*templates/index-310.hbs',
                to:'/templates',
                name:'index.hbs'
            }
        ]
    },
    {
        folder:"351_Branded_Require_Payment",
        config:"config-351.js.template",
        files:[
            // Standard
            '*css/normalise.css',
            '*css/style.css',
            '*img/background-lg.jpg',
            '*img/background-sm.jpg',
            '*img/lgo-tomizone.svg',
            '*partials/_footer.hbs',
            '*application.hbs',
            '*templates/error.hbs',
            '*templates/terms.hbs',

            // Account templates
            '*templates/account.hbs',
            '*templates/newaccount.hbs',
            '*templates/payments.hbs',
            '*templates/resetemailsent.hbs',
            '*templates/resetpass.hbs',
            '*templates/voucher.hbs',
            '*templates/forgot.hbs',

            // Account views
            '*views/rate.hbs',
            '*views/rates.hbs',
            '*views/userpayment.hbs',

            // Account js
            '*js/rates.js',
            '*js/spin.js',
            '*js/spinnerbutton.js',

            // Account components
            '*components/spin-spinner.hbs',
            '*components/spinner-button.hbs',

            // Account css
            '*css/account.css',

            // exclude
            '!**/gulpfile.js',
            '!**/package.json',
            '!**/node_modules/*'
        ],
        renameFiles:[
            // Header Account partial
            {
                from:'*partials/_header-account.hbs',
                to:'/templates',
                name:'_header.hbs'
            },
            // Success template
            {
                from:'*templates/success-351.hbs',
                to:'/templates',
                name:'success.hbs'
            }
        ]
    },
    {
        folder:"360_Branded_Free_Then_Require_Payment",
        config:"config-360.js.template",
        files:[
            // Standard
            '*css/normalise.css',
            '*css/style.css',
            '*img/background-lg.jpg',
            '*img/background-sm.jpg',
            '*img/lgo-tomizone.svg',
            '*partials/_footer.hbs',
            '*application.hbs',
            '*templates/error.hbs',
            '*templates/terms.hbs',

            // Account templates
            '*templates/account.hbs',
            '*templates/newaccount.hbs',
            '*templates/payments.hbs',
            '*templates/resetemailsent.hbs',
            '*templates/resetpass.hbs',
            '*templates/voucher.hbs',
            '*templates/forgot.hbs',

            // Account views
            '*views/rate.hbs',
            '*views/rates.hbs',
            '*views/userpayment.hbs',

            // Account js
            '*js/rates.js',
            '*js/spin.js',
            '*js/spinnerbutton.js',

            // Account components
            '*components/spin-spinner.hbs',
            '*components/spinner-button.hbs',

            // Account css
            '*css/account.css',

            // Advertising css
            '*css/adds.css',

            // Advertising images
            '*img/300-ad.png',
            '*img/960-ad.png',

            // exclude
            '!**/gulpfile.js',
            '!**/package.json',
            '!**/node_modules/*'
        ],
        renameFiles:[
            // Header Account partial
            {
                from:'*partials/_header-account.hbs',
                to:'/templates',
                name:'_header.hbs'
            },
            // Success template
            {
                from:'*templates/index-360.hbs',
                to:'/templates',
                name:'index.hbs'
            },
            // Success template from 351
            {
                from:'*templates/success-351.hbs',
                to:'/templates',
                name:'success.hbs'
            }
        ]
    },
    {
        folder:"410_Branded_Require_Profile",
        config:"config-410.js.template",
        files:[
            // Standard
            '*css/normalise.css',
            '*css/style.css',
            '*img/background-lg.jpg',
            '*img/background-sm.jpg',
            '*img/lgo-tomizone.svg',
            '*partials/_header.hbs',
            '*partials/_footer.hbs',
            '*application.hbs',
            '*templates/error.hbs',
            '*templates/terms.hbs',

            // Progressive profiling
            '*js/app-progressive.js',

            // spinner js
            '*js/spin.js',
            '*js/spinnerbutton.js',

            // spinner components
            '*components/spin-spinner.hbs',
            '*components/spinner-button.hbs',


            // exclude
            '!**/gulpfile.js',
            '!**/package.json',
            '!**/node_modules/*'
        ],
        renameFiles:[
            // Index template
            {
                from:'*templates/index-410.hbs',
                to:'/templates',
                name:'index.hbs'
            },
            // Success template from 111
            {
                from:'*templates/success-111.hbs',
                to:'/templates',
                name:'success.hbs'
            }
        ]
    },
    {
        folder:"421_Branded_Require_Profile_And_ROKT_Offer",
        config:"config-421.js.template",
        files:[
            // Standard
            '*css/normalise.css',
            '*css/style.css',
            '*img/background-lg.jpg',
            '*img/background-sm.jpg',
            '*img/lgo-tomizone.svg',
            '*partials/_header.hbs',
            '*partials/_footer.hbs',
            '*application.hbs',
            '*templates/error.hbs',
            '*templates/terms.hbs',

            // Progressive profiling
            '*js/app-progressive.js',

            // spinner js
            '*js/spin.js',
            '*js/spinnerbutton.js',

            // spinner components
            '*components/spin-spinner.hbs',
            '*components/spinner-button.hbs',


            // exclude
            '!**/gulpfile.js',
            '!**/package.json',
            '!**/node_modules/*'
        ],
        renameFiles:[
            // Index template from 410
            {
                from:'*templates/index-410.hbs',
                to:'/templates',
                name:'index.hbs'
            },
            // Success template from 111
            {
                from:'*templates/success-421.hbs',
                to:'/templates',
                name:'success.hbs'
            }
        ]
    },
    {
        folder:"430_Branded_Require_Profile_And_Video",
        config:"config-430.js.template",
        files:[
            // Standard
            '*css/normalise.css',
            '*css/style.css',
            '*img/background-lg.jpg',
            '*img/background-sm.jpg',
            '*img/lgo-tomizone.svg',
            '*partials/_header.hbs',
            '*partials/_footer.hbs',
            '*application.hbs',
            '*templates/error.hbs',
            '*templates/terms.hbs',

            // Progressive profiling
            '*js/app-progressive.js',

            // spinner js
            '*js/spin.js',
            '*js/spinnerbutton.js',

            // spinner components
            '*components/spin-spinner.hbs',
            '*components/spinner-button.hbs',

            // Video-js
            '*css/video-js.css',
            '*css/video.css',
            '*js/video.js',

            // Videos
            '*video/big_buck_bunny.mp4',
            '*video/big_buck_bunny.ogv',
            '*video/big_buck_bunny.webm',

            // Video poster image
            '*img/big_buck_bunny-poster.jpg',


            // exclude
            '!**/gulpfile.js',
            '!**/package.json',
            '!**/node_modules/*'
        ],
        renameFiles:[
            // Index template from 410
            {
                from:'*templates/index-410.hbs',
                to:'/templates',
                name:'index.hbs'
            },
            // Success template from 221
            {
                from:'*templates/success-221.hbs',
                to:'/templates',
                name:'success.hbs'
            }
        ]
    }
];

//--------------------------------------------------------------------------------------------------
// Theme compilation
//--------------------------------------------------------------------------------------------------


gulp.task('clean', function (cb)
{
    // Clean the tmp folder out
    return del('./build/**/*', cb);
});


function buildCopyTask(files, destination, callback)
{
    gulp.src(files)
        .pipe(gulp.dest('./build/'+destination))
        .on('end', function() {
            callback();
        })
        .on('error', function(err) {
            callback(err);
        });
}

function buildCopyRenameTask(files, destination, callback)
{
    async.each(files, function(file, callback) {
        gulp.src(file.from)
            .pipe(rename(file.name))
            .pipe(gulp.dest('./build/'+destination+file.to))
            .on('end', function() {
                callback();
            })
            .on('error', function(err) {
                callback(err);
            });
    }, function(err)
    {
        // if any of the file processing produced an error, err would equal that error
        if( err ) {
            // One of the iterations produced an error.
            // All processing will now stop.
            console.log('A file failed to process');
        } else {
            console.log('All files have been processed successfully');
        }
        callback(err);
    });

}

function buildCopyConfigTask(config, destination, callback)
{
    gulp.src('./js/'+config)
        .pipe(rename("config.js"))
        .pipe(gulp.dest('./build/'+destination+'/js'))
        .on('end', function() {
            callback();
        })
        .on('error', function(err) {
            callback(err);
        });
}

function buildZipTask(destination, callback)
{
    gulp.src('./build/'+destination+'/**/*')
        .pipe(zip(destination+'.zip'))
        .pipe(gulp.dest('./build'))
        .on('end', function() {
            callback();
        })
        .on('error', function(err) {
            callback(err);
        });
}

gulp.task('build-all', function (cb)
{
    async.each(themes, function(theme, callback) {

        // Perform operation on file here.
        console.log('Processing theme ' + theme.folder);

        async.waterfall([
                function (callback) {
                    buildCopyTask(theme.files, theme.folder, callback);
                },
                function (callback) {
                    buildCopyRenameTask(theme.renameFiles, theme.folder, callback);
                },
                function (callback) {
                    buildCopyConfigTask(theme.config, theme.folder, callback);
                },
                function (callback) {
                    buildZipTask(theme.folder, callback);
                }
            ],
            function (err)
            {
                if (err)
                {
                    console.error(err);
                    callback(err);
                }
                else
                {
                    console.log('Processed theme ' + theme.folder);
                    callback();
                }
            });

    }, function(err){
        // if any of the file processing produced an error, err would equal that error
        if( err ) {
            // One of the iterations produced an error.
            // All processing will now stop.
            console.log('A file failed to process');
        } else {
            console.log('All files have been processed successfully');
        }
        cb(err);
    });
});



//--------------------------------------------------------------------------------------------------
// Default build
//--------------------------------------------------------------------------------------------------

gulp.task('default', function (cb)
{
    runSequence(
        'clean',
        'build-all',
        cb);
});

gulp.task('release', function (cb)
{
    runSequence(
        'clean',
        'build-all',
        'bumpVersion',
        'git-cleanup', // Run this first to remove and prev git worktrees
        'git-commit-themes',
        'git-cleanup',
        cb);
});

//--------------------------------------------------------------------------------------------------
// Release changes
//--------------------------------------------------------------------------------------------------

var bump =              require('gulp-bump');
var git =               require('gulp-git');
var filter =            require('gulp-filter');
var tag_version =       require('gulp-tag-version');
var tap =               require('gulp-tap');
var exec =              require('child_process').exec;
var async =             require('async');
var fs =                require('fs');

var getPackageVersion = function () {
    return JSON.parse(fs.readFileSync('./package.json', 'utf8')).version;
};
var appVersion = getPackageVersion();
var getAppVersion = function (reload) {
    if (reload || !appVersion) {
        appVersion = getPackageVersion();
    }
    return appVersion;
};

gulp.task('bumpVersion', function () {
    return gulp.src(['./package.json'])
        .pipe(bump({type: 'minor'}))
        .pipe(gulp.dest('./'))
        .pipe(git.commit('[gulp-tag-version] bumps package version'))
        .pipe(filter('package.json'))
        .pipe(tag_version())
        .pipe(tap(function () {
            getAppVersion(true);
        }));
});

// Major version number get increased for each sprint.
// Run this task at the begining of each sprint.
gulp.task('updateSprintVersion', function () {
    return gulp.src(['./package.json'])
        .pipe(bump({type: 'major'}))
        .pipe(gulp.dest('./'))
        .pipe(git.commit('[gulp-bump] bumps package version'));
});

gulp.task('git-commit-themes', function (cb)
{
    async.each(themes, function(theme, callback) {

        async.waterfall([
                function (callback) {
                    console.log('Checking out branch ./build/GIT-'+theme.folder);
                    git.exec({args : 'worktree add ./build/GIT-'+theme.folder+' '+theme.folder}, function (err, stdout) {
                        if(stdout)
                            console.log(stdout)
                        callback(err);
                    });
                },
                function (callback) {
                    console.log('Copying source '+theme.folder+' to ./build/GIT-'+theme.folder);
                    gulp.src('./build/'+theme.folder+'/**/*')
                        .pipe(gulp.dest('./build/GIT-'+theme.folder))
                        .on('end', function() {
                            callback();
                        })
                        .on('error', function(err) {
                            callback(err);
                        });
                },
                function (callback) {
                    console.log('GIT Adding source ./build/GIT-'+theme.folder);
                    git.exec({
                            args: 'add --all',
                            cwd: './build/GIT-'+theme.folder
                        }, function (err, stdout) {
                            if(stdout)
                                console.log(stdout)
                            callback(err);
                        });
                },
                function (callback) {
                    console.log('GIT Commiting source ./build/GIT-'+theme.folder);
                    git.exec({
                        args: 'commit -a -m "Automated build commit"',
                        cwd: './build/GIT-'+theme.folder
                    }, function (err, stdout) {
                        if(stdout)
                            console.log(stdout)
                        callback(err);
                    });
                },
                function (callback) {
                    console.log('GIT Pushing source ./build/GIT-'+theme.folder);
                    git.exec({
                        args: 'push',
                        cwd: './build/GIT-'+theme.folder
                    }, function (err, stdout) {
                        if(stdout)
                            console.log(stdout)
                        callback(err);
                    });
                }
            ],
            function (err, values) {
                console.log('end');
                if (err)
                {
                    console.error(err)
                    cb(err);
                }
                else
                {
                    console.log('success');
                    cb();
                }
            });

    }, function(err){

        if( err )
        {
            console.error(err)
            console.log('A file failed to delete');
        } else {
            // Git worktree prune
            console.log('All files have been processed successfully');
        }
        cb(err);
    });
});


gulp.task('git-cleanup', function (cb)
{
    async.each(themes, function(theme, callback) {

        // Perform operation on file here.
        console.log('Cleaning up theme ' + theme.folder);
        del([
                './build/GIT-'+theme.folder+'/**/*',
                './build/GIT-'+theme.folder
            ], function(result){
                console.log('deleted');
                callback();
            },
            function(err){
                console.log('errored');
                callback(err)
            });

    }, function(err){
        git.exec({args : 'worktree prune'}, function (err, stdout) {
            if( err ) {
                console.log('A file failed to delete');
            } else {
                // Git worktree prune
                console.log('All files have been processed successfully');
            }
            cb(err);
        });
    });

});


