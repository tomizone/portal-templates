#Tomizone Portal Templates

A collection of templates for the Tomizone WiFi Portal.

## Languages

We use the [Handlebars](http://handlebarsjs.com) (HBS) templating engine with HTML5/CSS3/Javascript.

When you upload your finished template to our API we compile the HBS templates into an Ember.js file and distribute the compiled Javascript on our Content Delivery Network (CDN) and the router for blisteringly fast User Experience.


## Mobile First, Modern Browsers

The vast majority of devices on Public WiFi networks (more than 85%) are mobile, namely iOS and Android. You might want to include support for old versions of Internet Explorer, but these generally account for less than 1% of traffic.

If you're starting from scratch we recommend using [Bootstrap](http://www.getbootstrap.com) to get started. Our templates are often hand crafted in the persuit of size and speed.

## Running NodeJS builds

This is the source code that creates examples of themes. Builds use [NodeJS](https://nodejs.org/en/).

After installing node js run the following command in the ./ directory to get the development environment set up:

```
npm install
```

All templates are built out to a ./build directory. To run the build run the following command in the ./ directory:

```
gulp
```